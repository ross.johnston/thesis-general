import tkinter as tk
from tkinter import filedialog

def folder_selection(preset=None):
    # function for finding a folder path and storing it as a global variable called path_data
    if preset is None:
        def browse_for_folder():
            # Create a root window (hidden)
            root = tk.Tk()
            root.withdraw()
        
            # Open a folder explorer dialog
            folder_path = filedialog.askdirectory()
    
        
            # Close the root window (optional)
            root.destroy()
            return folder_path
        
        # Create a simple GUI for the folder selection
        main_window = tk.Tk()
        main_window.title("Find the folder for the data")
        
        label = tk.Label(main_window, text="Click the button to select a folder:")
        label.pack(pady=10)
        button = tk.Button(main_window, text="Browse", command=lambda: set_selected_folder(browse_for_folder()))
        button.pack(pady=10)
        
        def set_selected_folder(folder_path):
            global path_data
            path_data = folder_path
            main_window.destroy()
        
        main_window.mainloop()
    else:
        global path_data
        path_data = preset

    print("path_data variable is set to : " + path_data)
    return path_data




