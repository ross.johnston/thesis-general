# Notes for using this repository as a shared code, submodule folder, for the other repositories


In the Chapters folder, which wants to make use of this add:

  git submodule add \<url-to-shared-repo\> \<path-to-shared-subdirectory\>


Whenever you want to update version of shared code in the specified chapter you must run:

  git submodule update --remote


If you accidentally change the shared code in the chapter, you can reset it to the most recent commit by running:
  
  cd \<path-to-shared-subdirectory\>
  git reset --hard HEAD

# Notes on repositories in general

The commits will generally be made, going forward, in reference to the changes made to notebook cells. Keeping these commits as frequent as possible will allow me to easily recover a particular version of my analysis without having an overly crowded current notebook.