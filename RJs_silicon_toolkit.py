import scipy.constants as c
import numpy as np
import math as m
import ruamel.yaml as ryaml
import os
from scipy.interpolate import interp1d

# Handle materials

class material_property_handler:
    """
    Ideally this could be a more generalised class which could work with the empirical material properties data and pull it for particular temperatures, wavelengths etc.

    However, for now it will just be a class to handle the specific heat data for silicon

    INPUTS
    ------
    database_path: path to the database file

    OUTPUTS
    ------
    c_T: specific heat [J kg^-1 K^-1]

    EXAMPLE USES
    -------
    # 1). plot the interpolated c_T curve

    database_path = 'path/to/database'
    mat_Si = material_property_handler(database_path)
    fig, ax = plt.subplots(figsize=(8,6))
    range_T = np.linspace(2,350,100)
    ax.plot(range_T,mat_Si.c_T(range_T),label='Combined',linestyle='--',color='k')
    ax.set_xlabel('Temperature (K)')
    ax.set_ylabel('Specific Heat (J kg$^{-1}$ K$^{-1}$)')
    ax.legend()
    """
    def __init__(self,database_path):
        path_silicon_database = os.path.join(database_path,'silicon_properties.yaml')
        with open(path_silicon_database, 'r') as file:
          silicon_properties = ryaml.safe_load(file)
        self.data = silicon_properties

    def c_T(self, T):
        # Ensure T is a numpy array
        # c_T
        # lets just write a function which will pick either curve with a switch at 250K for now and then we can make it more sophisticated later  
        c_T_highT = np.asarray(self.data['Thermal']['Specific Heat']['data'])
        c_T_lowT = np.asarray(self.data['Thermal']['Specific Heat 3']['data'])
        c_T_highT[:,1] *= 1e3 # convert to J kg^-1 K^-1
        c_T_lowT[:,1]*= 1e3 # convert to J kg^-1 K^-1
        self.range_highT_T = np.linspace(np.min(c_T_highT[:,0]),np.max(c_T_highT[:,0]),100)
        self.range_lowT_T = np.linspace(int(np.min(c_T_lowT[:,0]))+1,np.max(c_T_lowT[:,0]),100)
        self.c_T_highT_interp = interp1d(c_T_highT[:,0],c_T_highT[:,1],kind='cubic')
        self.c_T_lowT_interp = interp1d(c_T_lowT[:,0],c_T_lowT[:,1],kind='cubic')
        self.T_threshold = 250

        T = np.array(T)
        
        # Initialize an empty array for the results, same shape as T
        results = np.empty_like(T,dtype=float)
        
        # Apply conditions and interpolations
        lowT_mask = T < self.T_threshold
        highT_mask = ~lowT_mask  # Inverse of lowT_mask
        
        # Interpolate based on conditions
        results[lowT_mask] = self.c_T_lowT_interp(T[lowT_mask])
        results[highT_mask] = self.c_T_highT_interp(T[highT_mask])
        
        return results


# Parse data from sample-database.yaml


def query_database(database_path,study=None,default_value_fill=0):
        """ Fetches data/ infor from the PTD database
    
        Parameters
        ----------

        
        """

        with open(database_path, 'r') as file:
            sample_database = ryaml.safe_load(file)
        
        silicon_database = sample_database['silicon']

        if study == None:
            study_list = list(silicon_database.keys())
            print('Please select a study from the following list:')
            for i,study in enumerate(study_list):
                print('{0}: {1}'.format(i,study))

        else:
            study = silicon_database[study]  
            exclusion_list = ['dopant-type', 'dopant', 'data-link','resistivity-specified','measurement-list']   
            for key in study.copy().keys():
                if key in exclusion_list:
                   study.pop(key)
            # For every key in study, check the dictionary of values and if there are any values missing from one key which are in another then add them with a default value of default_value_fill

            sample_inclusion_list = []
            for sample_key in study.keys():
                for data_key in study[sample_key].keys():
                    if data_key not in sample_inclusion_list:
                        sample_inclusion_list.append(data_key)

            for sample_key in study.keys():
                for data_key in sample_inclusion_list:
                    if data_key not in study[sample_key].keys():
                        study[sample_key][data_key] = default_value_fill

            return study
        """
            # Lets iterate over the supplied wavelengths
            for wv in wavelength:
                # Lets iterate over the supplied dates
                for day in dates:
                    try:
                        data_wv = self.deflection_database[wv]
                    except:
                        print('!!! Wavelength: {0} not found in database'.format(wv))
                        continue
                    try:
                        data_date = data_wv[self.sample_properties['type']][day]
                    except:
                        print('!!! Date: {0} not found in database'.format(day))
                        continue
                    for scan in data_date:
                        scan_data = data_date[scan]
                        if scan_data['sample_name'] == sample and scan_data['scan_type'] == scan_type:
                            scan_name = str(day) + '-' + str(scan)
                            data_dic[scan_name] = scan_data
                            data_dic[scan_name]['wavelength'] = wv
                            beam_position = mat(data_dic[scan_name]['beam_origin'])
                            sample_rotation = data_dic[scan_name]['rotation']
                            if data_dic[scan_name]['facing'] == 'front':
                                beam_position = beam_position.T*mat([[m.cos(sample_rotation), m.sin(sample_rotation)],[-m.sin(sample_rotation), m.cos(sample_rotation)]]) # Counter clockwise rotation
                            elif data_dic[scan_name]['facing'] == 'back':
                                beam_position = beam_position.T*mat([[m.cos(sample_rotation), -m.sin(sample_rotation)],[m.sin(sample_rotation), m.cos(sample_rotation)]]) # Clockwise rotation
                            data_dic[scan_name]['beam_position'] =  beam_position
                            scan_path = self.deflection_data_path+prefix+scan_name+'s.nat'
                            data_dic[scan_name]['file_path'] = ''
                            if scan_type == '2D':
                                # This is a 2D measurement, so I need to load the data differently
                                try:
                                    with open(scan_path, 'r') as f:
                                        lines = f.readlines()
                                    data_dic[scan_name]['file_path'] = scan_path
                                except:
                                    print('No scan file found at {0}'.format(scan_path))
                                    continue

                                pattern = r'\]'
                                sep = np.asarray([i for i,line in enumerate(lines) if re.findall(pattern, line)==[']']])
                                test = np.asarray([x/(i+1) for i,x in enumerate(sep[1:]/sep[1])])
                                uneven_steps = len(np.where(test!=1)[0])                  
                                if uneven_steps > 0:
                                    print('Uneven number of steps in z detected, please check data file')
                                z_steps = (sep[1]-sep[0])-2
                                try:
                                    with open(scan_path, 'r') as f:
                                        # To load the file I need to know What the number of steps are in z
                                        lines = (line for i, line in enumerate(f) if i!=0 and (i+1) % (z_steps+2) != 0)
                                        lines_short = (line for i, line in enumerate(lines) if (i+1) % (z_steps+1) != 0)
                                        full_scan_data = np.asarray(np.genfromtxt(lines_short))
                                        # Tidy up the motor positions
                                        full_scan_data[:,0] = self.tidy_spacing(full_scan_data[:,0],sig=3)
                                        full_scan_data[:,1] = self.tidy_spacing(full_scan_data[:,1],sig=3)
                                        if 'power_correction' in scan_data:
                                            full_scan_data[:,2] = full_scan_data[:,2]*scan_data['power_correction']
                                            print('Power correction applied')
                                except:
                                    print('Error loading file! Make sure there is the same number of rows for each step ')
                                    full_scan_data = [0]

                            else:
                                try:
                                    full_scan_data = np.loadtxt(scan_path)
                                    full_scan_data[:,0] = self.tidy_spacings(full_scan_data[:,0],sig=3)
                                    if 'power_correction' in scan_data:
                                        full_scan_data[:,1] = full_scan_data[:,1]*scan_data['power_correction']
                                    data_dic[scan_name]['file_path'] = scan_path
                                except:
                                    print('No 1D scans found at {0}. Continuing onto different scan'.format(scan_path))
                                    continue
                                sample_length = self.sample_properties['length'] # mm
                                sample_length_scale = sample_length/self.n
                                effective_sample_length = round(sample_length_scale) + 5 # mm is rough just now
                                ind_cutoff = np.where(full_scan_data[:,0] > full_scan_data[:,0][0] + effective_sample_length)[0][0]
                                full_scan_data = full_scan_data[:ind_cutoff,:]
                                #full_scan_data[:,1]*= sample_specs[sample]['probe_correction'] # !Probe correction not neeed as Angus explained
                            
                            data_dic[scan_name]['scan_data'] = full_scan_data
            self.data_dic = data_dic
            return self.data_dic
        else:
            print('Please provide a sample, wavelength and date to query the database. Not updating the cached data dic either')
            return None
        """




# Empirical models for absorption in Silicon

def a_sts(wv,n,p):
    """
    SOURCE: schroderFreeCarrierAbsorption1978

    INPUT:
    n = n_type_carrier_density [cm^2]
    p = p_type_carrier_density [cm^2]
    wv = wavelength (array or point) [um]

    OUTPUT:
    a = absorption [cm^-1]

    """
    a = np.power(wv,2)*(n*1e-18 + p* 2.7e-18)
    return a

def a_green(wv,n,p):
    """
    SOURCE: chin-yitsaiInterbandIntrabandAbsorption2020

    Converted to working with units of m

    INPUT:
    n = n_type_carrier_density [cm^2]
    p = p_type_carrier_density [cm^2]
    wv = wavelength (array or point) [um]

    OUTPUT: 
    a = absorption [cm^-1]
    """
    a = np.power(wv,3)*n*2.6e-18 + np.power(wv,2)*p*2.7e-18
    return a

##  Resistivity 

# degallaixBulkOpticalAbsorption2013 shows an empirical trend for the absorption at 1550nm due to the excitation of the n-type 2300nm band: 

def abs_empirical_2013(R):
    absorption = 0.0454/R
    err = (7e-4)/R
    return absorption,err

# Analytical Models for absorption contributions in Silicon DOI: 10.1002/0470021942

# Fundamental: Interband Band-Band transitions 

def n_eff(T,m_eff):
    n_eff = 2*np.power(2*c.pi*m_eff*c.boltzmann*T,3/2)/(c.h**3)
    return n_eff

def n_intrinsic(T,n_e_eff,n_h_eff,e_g):
    """
    intrinsic carrier concentration
    T: temperature
    n_e_eff: effective electron density
    n_h_eff: effective hole density
    e_g: bandgap energy
    """
    n_intrinsic = np.sqrt(n_e_eff*n_h_eff)*np.exp(-e_g/(2*c.boltzmann*T))

def eg(T, eg0, a, b):
    """
    empirical bandgap model
    T: temperature
    eg0: bandgap energy at 0K
    eg0Si = 1.166 # eV
    aSi = 4.73e-4 # eV/K
    bSi = 636 # K
    """
    y = eg0 - a*T*T/(T+b)
    return y

def abs_interband(v_array,a0,Eg,Eu):

    # Energies should all be input in units joules

    abs_array = a0*np.pow(m.e,(c.h*v_array-Eg)/Eu)
    return abs_array 

def direct_b2b(v_array,n,me_reduced,Eg):
    A = pow(c.e/c.Planck,2)*(1/(n*c.c*me_reduced))*pow(2*me_reduced,1.5)
    alpha_array = A*np.sqrt(c.Planck*v_array-Eg)/(c.Planck*v_array)
    return alpha_array

def indirect_b2b(v_array,n,me_reduced,Eg,T):
    A = pow(c.e/c.Planck,2)*(1/(n*c.c*me_reduced))*pow(2*me_reduced,1.5)
    f_BE = 1/ (np.power(c.e,(c.Planck*v_array)/(c.Boltzmann*T))-1)
    below_Eg = A*f_BE*np.power(2*c.Planck*v_array-Eg,2)
    above_Eg = A*(1-f_BE)*np.power(Eg,2)
    return

# Subband transition

## Free carrier absorption

#def ntype_carrier()


def FCA_drude(N,q,m_eff,n,omg,tau):
    """
    DOI: 10.1088/1361-6382/ab9143

    INPUTS
    ------
    N - Carrier density
    q - electron charge
    m_eff -  effective carrier mass
    n - refractive index 
    tau - Carrier time scale
    omg - angular frequency

    COMMENTS
    --------
    Carrier density, mass, and mobility are different for electrons and holes.
    """
    absorption = N*pow(q,2)/(m_eff*c.epsilon_0*n*c.c*pow(omg,2)*tau)

    return absorption

def drude(q,wv,N,n,m_eff,mu):
    """
    INPUT
    -----
    q = charge of carrier 
    wv = wavelength [cm]
    N = carrier concentration [cm^-3]
    n = refractive index
    m_eff = effective mass [kg]
    mu = carrier mobility [cm^2/V/s]

    OUTPUT
    ------

    absorption [cm^-1]

    """
    a = pow(q,3)*pow(wv,2)*N/(4*pow(c.pi,2)*(100*c.epsilon_0)*pow(100*c.c,3)*n*pow(m_eff,2)*mu)
    
    return a

def tau(m_eff,mu,q):
    return m_eff*mu/q

## Two Photon Absorption (TPA)

def n_TPA(wv, tc, b_TPA,I):
    """
    Calculates the indueced carrier density due to TPA
    wv: wavelength [m] 
    tc: carrier lifetime [s]
    b_TPA: TPA coefficient [cm/W]
    I: laser intensity [W/cm^2]
    """
    return b_TPA*(I**2)*tc*wv/(2*np.pi*c.hbar*c.c)

def cross_section_a(wv,index,m_eff,mu):
    """
    Calculates the absorption cross section
    wv: wavelength [m]
    index: refractive index 
    m_eff: effective mass [kg] : M_eff_e = 0.26*m_e , m_eff_h = 0.36 *m_e
    mu: mobility [cm^2/Vs] : for lighlty doped samples  μe = 1400 cm2/V-s and μh = 500 cm2/V-s at 300k 
    """
    return (c.e**3)*(wv**2)/(4*c.pi*(c.c**2)*c.epsilon_0*index*(m_eff**2)*mu)

## @ Room temperature every dopant is ionised & so resistivity => free carrier: 

def carrier_density(q,mu,rho):
    """
    Calculates the carrier density
    q: charge of carrier
    mu: mobility [cm^2/Vs]
    rho: resistivity [ohm-cm]
    """    
    #Typical mobility mu for the holes (p-type) or electrons (n-type)?

    return 1/(q*mu*rho)

##

def r_SRH(n_i,n_e,n_e1,t_e,n_h,n_h1,t_h):
    """
    SRH recombination rate
    n_i: intrinsic carrier concentration
    n_e: electron concentration
    n_e1: electron concentration change
    t_e: minority electron lifetime
    n_h: hole concentration
    n_h1: hole concentration change
    t_h: minority hole lifetime
    """
    R = (n_e*n_h - (n_i**2))/(t_h*(n_e + n_e1) + t_e*(n_h + n_h1))
    return R

def t_minority(T,m_eff,sig,n_T):
    """
    minority carrier lifetime
    T: temperature
    m_eff: effective mass
    sig: Capture cross section
    n_T: number density of deep level state
    """
    v_th = np.sqrt(3*c.k*T)/m_eff
    t_minority = 1/(sig*n_T*v_th)
    return t_minority

"""
# LPE growth  

# p: resistivity
# mu: mobility cm^2/V/s
# T: temperature K
# tau: lifetime s

# Effective masses # http://ecee.colorado.edu/~bart/book/effmass.htm 

tau = 10e-6 # s #DOI: 10.1007/978-3-319-48933-9
Cp = 1.5 # really 1 - 3 × 10 -31 cm6/sec
Nd = m.sqrt(1/(Cp*tau)) # DOI: 10.1016/0038-1101(83)90173-9

me = 1.08*c.m_e # effective mass of the electron from Angus
mp = 0.81*c.m_e # effective mass of the hole from Angus
mu = 1e4

p = 1/(c.elementary_charge*mu*Nd)
print(p)

"""
# Check your units for every property

def mass_supported_crystalline_neck(d,sig,g):
    """
    Size limits (kg) of a neck pulled Silicon DOI: 10.1016/0022-0248(90)90253-H

    INPUT
    d: diameter of neck
    sig: fracture strength dyn/cm^2 <=> 0.1 Pa
    g: gravity acceleration cm/s^2

    OUTPUT
    size: [kg] # It is clear that with a larger 12mm neck, stabilised against dislocations with B-doping that a 2000kg crystal can be pulled which is x10 above the target of future detectors & so this will not be an issue.

    EXAMPLE
    sig = 2e9 # 0.2 GPa which is fairly standard for Silicon DOI: 10.1016/B978-0-444-82413-4.50060-3
    g = 981
    rho = 2.33e3


    """
    return (sig*c.pi*d**2)/(4*g*1000)