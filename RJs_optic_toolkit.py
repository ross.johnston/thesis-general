
from __future__ import print_function
from distutils.log import error
from mpmath import nsum, exp, inf, power,factorial, gamma, gammainc
import scipy.constants as c
import re
import os
import cmath as cm
import numpy as np
import math as m
import scipy.integrate as integrate
from scipy import special as sp
#import tmm # pip install tmm
from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets
import sympy.physics.optics as opt
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from sympy import Matrix as mat


# Unit equations

def disp_units(value, unit):
    units = [['n',1e-9],['u',1e-6],['m',1e-3],['k',1e+3],['M',1e+6]]
    select = {}
    for x in units:
        select[x[0]] = x[1]
    return str(value/select[unit])+ ' '+unit 

def conv_units(value, pref0, pref1,power=1):
    prefs = np.asarray([['n',1e-9],['u',1e-6],['m',1e-3],['c',1e-2],['1',1],['k',1e+3],['M',1e+6],['G',1e9]])
    pref_vals = prefs[:,1].astype(float)
    ind0 = np.where(prefs == pref0)[0][0]
    ind1 = np.where(prefs == pref1)[0][0]
    value_conv = value*(pref_vals[ind0]/pref_vals[ind1])**power
    print(value_conv)
    select = {}
    for x in prefs:
        select[x[0]] = x[1]
    return value_conv

# Plotting tools



# Optic equations

def waist2pow(r,w):
    pr = 1-m.pow(m.e,-2*(r/w)**2)
    return pr

def z2waist(wv,w0,z):
    zr = m.pi*(w0**2)/wv
    return w0*m.sqrt(1+(z/zr)**2)


def pow2waist(app,pin,pout):
    w = complex(0,1)*m.sqrt(2)*app/(cm.sqrt(cm.log(1-(pout/pin))))
    return w.real
    

def waist(wv,zr):
    return m.sqrt((zr*wv)/m.pi)
    
def radius(w0,z,zr):
    return w0*m.sqrt(1+(z/zr)**2)

def radius_curvature(z,zr):
    return z*(1 + pow(zr/z,2))

def intensity(radius,power):
    # Intensity of a gaussian beam in W/m^2
    return power/(m.pi*np.power(radius,2))

        

def Aperture_offset(a,w,d_range):
    """Calculate power drop from 1D Aperture offsets
    INPUTS
    a : aperture width
    w : beam waist
    d_range : np.array of offsets

    OUTPUTS
    Scaled power 
    """
    Poff = np.asarray([[d,float(nsum(lambda x: power(2,x)*power(d,2*x)*gammainc(x+1,0,2*(a/w)**2)/(power(w,2*x)*factorial(x)**2), [0, inf]))*np.exp(-2*(d/w)**2)] for d in d_range])
    return Poff




# Math equations


def rad2deg(rad):
    return rad*180/m.pi

def gauss(x,x0,a,sigma):
    return (1/(a*sigma*m.sqrt(2*m.pi)))*np.exp(-(x-x0)**2/(2*sigma**2))

def sample_poly(x, *coeffs):

    """
    EXAMPLE:

    p_opt, p_cov = curve_fit(fit_func,X_data,Y_data,p0=list(coeffs))
    """
    y = np.polyval(coeffs, x)
    return y

# Fresnel relations 

"""
- Normal (Ni) or Oblique (Oi) incidence
- Incoherent (Inc) or Coherent (Coh)
- Optically thick (Tk) or thin (Tn)

"""


def R_Ni(na,nb,ka=0,kb=0):
    # Reflection @ Normal incidence 
    return (np.power(na-nb,2) + np.power(ka-kb,2))/(np.power(na+nb,2) + np.power(ka+kb,2))

def R_Oi(na,nb,tht,pol='p'):
    """
    No absorption is considered here. From Optical Effects in Solids. 

    INPUTS
    na : refractive index of medium a
    nb : refractive index of next medium b
    tht : angle of incidence in degrees
    pol : polarisation of the light (p or s)
    """
    tht = np.radians(tht) # convert to radians

    if pol =='s':
        R = np.power((na*np.cos(tht)-nb*np.sqrt(1-np.power(na*np.sin(tht)/nb,2)))/(na*np.cos(tht)+nb*np.sqrt(1-np.power(na*np.sin(tht)/nb,2))),2)
    elif pol =='p':
        R = np.power((na*np.sqrt(1-np.power(na*np.sin(tht)/nb,2))-nb*np.cos(tht))/(na*np.sqrt(1-np.power(na*np.sin(tht)/nb,2))+nb*np.cos(tht)),2)
    else:
        print('Choose either p or s polarisation')
        error
    return R

def R_Oi_Tk(nb,k,tht,wv,pol='p'):
    """
    This accounts for absorption and input angle in the transmission and so should be compared to R_oi 
    I think the s-pol model is wrong
        Source: 10.1007/s00340-009-3611-z

    doesn't work for different na & nb options
    """
    tht = np.radians(tht)
    if type(k) != np.ndarray:
        k = np.asarray([k])
    power = np.power(nb+k*1j,2)
    cm_modifier = np.asarray([[cm.sqrt(power_i - pow(m.sin(tht),2))] for power_i in power])
    cm_modifier = np.squeeze(cm_modifier)
    if pol == 'p':
        top = power*np.cos(tht) - cm_modifier   
        bottom = power*np.cos(tht) + cm_modifier
        R = np.power(np.absolute(top/bottom),2)
    elif pol=='s':
        top = np.cos(tht) - cm_modifier
        bottom = np.cos(tht) + cm_modifier
        R = np.power(np.absolute(top/bottom),2)
    else:
        print('Polarisaiton needs to be either p or s')
        raise ValueError
    return R

def Light_Inc_array(R_ab,R_ba,nb,a,d,tht1):
    """

    SOURCE/ tannerOpticalEffectsSolids & ogusuSimpleAccurateMeasurement2006

    DESCRIPTION /

    Do Oblique angles away from the normal break the formulas (only change R?)
    na = 1 so only works for air-silicon 
    
    Surely at high angles R_ab != R_ba so the formula will breakdown?

    Hard to go from A [%] back to a [cm^-1] as the formula is not invertible (unless empirical relation is formed for fixed d)

    INPUTS /
    R_ab : Reflection coefficient at boundary a-b. 
    nb : Refractive index of boundary b
    a : Absorption coefficient
    d : Thickness of the layer
    tht1 : Incident angle in degrees

    OUTPUTS /
    list of Trans, Ref, Abs, Abs_direct, Abs_singlepass

    Abs_direct is the absorption coefficient calculated from?
    Abs_singlepass is the absorption coefficient calculated from a single pass through the layer
    """
    
    tht1 = np.radians(tht1)
    tht_2 = np.arcsin(np.sin(tht1)*1/nb) 
    d_app = d/(np.cos(tht_2))

    # Simplified model for N passes
    Trans = (np.power(1-R_ab,2)*np.exp(-a*d_app))/(1-np.power(R_ab,2)*np.exp(-2*a*d_app))
    Trans = Trans.astype(np.float64)
    Ref = R_ab*(1 + (np.power(1-R_ab,2)*np.exp(-2*a*d_app))/(1-np.power(R_ab,2)*np.exp(-2*a*d_app)))
    Ref = Ref.astype(np.float64)
    Abs = (1 - Ref - Trans )/d_app
    Abs_direct = np.log((1/Trans)*(1-np.power(R_ab,2))*(0.5 + np.sqrt(0.25 + (np.power(R_ab*Trans,2)/(np.power(1-R_ab,4))))))/d_app # does not seem to work
    
    # Progressive pass model

    Abs_singlepass = (np.power(1-R_ab,2)*np.exp(-a*d_app))

    return [Trans,Ref,Abs,Abs_direct,Abs_singlepass]



def Light_Inc(R_ab,nb,a,d,tht1):
    # !!! Don't use anymore

    # Do Oblique angles away from the normal break the formulas (only change R?)
    # na = 1 so only works for air-silicon 

    tht_2 = m.asin(m.sin(tht1)*1/nb)# radians # 1 = na
    d_app = d/(m.cos(tht_2))
    #tht2=tht1
    #d_app = d
    Trans =         (pow(1-R_ab,2)*exp(-a*d_app))/(1-pow(R_ab,2)*exp(-2*a*d_app))
    Ref = R_ab*(1 + (pow(1-R_ab,2)*exp(-2*a*d_app))/(1-pow(R_ab,2)*exp(-2*a*d_app)))
    Abs = (1 - Ref - Trans )/d_app
    Abs_direct = np.log((1/float(Trans))*(1-pow(R_ab,2))*(0.5 + m.sqrt(0.25 + (pow(R_ab*float(Trans),2)/(pow(1-R_ab,4))))))/d_app # does not seem to work
    return [float(Trans),float(Ref),float(Abs),float(Abs_direct)]

def d_app(tht1,na,nb,d):
    tht_2 = m.asin(m.sin(tht1)*1/nb)# radians # 1 = na
    d_app = d/(m.cos(tht_2))
    return d_app



def a_simple(R_ab, T,tht_1,d,na,nb):
    # Angus' way of using only the first transmission and reflection beams (w.r.t to boundarys a-b & b-c)
    # Correcting for apparent thicknes from incidence angle (tht): scale factor l/l_app for absorption
    # Absolutley correct providing beams can be seperated?
    tht_2 = m.asin(m.sin(tht_1)*na/nb)# radians
    d_app = d/(m.cos(tht_2))
    return [tht_2*180/m.pi,d/d_app,-m.log(T/pow(1-R_ab,2))/d_app]


def a_Tranmission(Pin, Pout, na,nb,tsub, tht1,pol='p',a_expect = 0 ,model='thick'):
    Rab_scaling = (1 - pow((na-nb)/(na+nb),2))
    Rab_angular_scaling = R_Oi(na,nb,tht1,pol)
    T = Pin/Pout
    tht2 = m.asin(m.sin(tht1)*na/nb)
    tapp = tsub/(m.cos(tht2))
    if model == 'thick':
        return -m.log(T/pow(Rab_scaling,2))/tapp
    if model == 'thin':
        b = (1-pow(Rab_scaling,2)/T)
        c = (0.5 + m.sqrt(0.25 + (pow(Rab_scaling*T,2))/pow(1-Rab_scaling,4)))
        print(b,c)
        return m.log(b*c)/tsub
    if model == 'thin2':
        # NO absorption here so its not valid
        T2 = 2*nb/(pow(nb,2)+1) 
        R2 = pow(nb-1,2)/(pow(nb,2)+1)
        return 1 - R2 -T2

    

def A_Coh(input_array,wl,pol,nsub,dsub,nc,k,d):
    """
    
    Example: 
    To optimse around experimental measurements of T,R different angles
    p_opt, p_cov = curve_fit(light_fit,power_array[:,0],abs_array,p0=[0.000008,1e-6])

    """
    d_list = ['inf'] + [d] + [dsub] + ['inf'] 
    n_list = [1] + [nc + complex(0,k)] + [nsub] + [1]
    light_array = [tmm.coh_tmm(pol,n_list,d_list,x,wl) for x in input_array]
    return [1-x['T']-x['R'] for x in light_array]

# Corrections for absorption measurements

def absorption_analysis(fname,data_loc,xl_range,parameter_space,measurement_type,bulk,k_analysis=True):

    """

    1.0 Python port of the Matlab 1.4 version produced by S. Tait & Z. Tornasi 

    Next version:

    Transmissivity wrongly assumes that Transmisvity is the same at the max as at the starting point? 
    However, as you move slightly horizontally over the sample surface and surely the p@max should be scaled relative to input?

    %check for rows with transmissivity out of bounds - applies for mapping?

    !Not done until time is properly added to excel / decompiled labview
    and the logic of a gradual drift is reliably checked:
    A linear interpolation between calibration points is also carried out to
    calculate the total error due to calibration drift the logic used in
    cdrift.m


    NEW Version:

    Requires the excel sheet to be in the same root folder as the python script & allows you to 
    specify the location of the data (the names of which are int he 1st column of the excel)

    For Conversion to 'k' values the user will be prompted to continue by a [Y/N]
    request. Where if enabled, the same measuremnets will be used to call
    codes written by Z.Tornasi and S.Leavey to calculate the Electric feild
    intensity inside the coating layer for a given paramater space.


    OLD VERSION:

    Function takes in a excel cell reference to an excel sheet that the user
    will be prompted to point the program to. This should be formatted using
    the example excel sheet shown in : BLA
    Any changes from this formatting will cause this function to break

    Using the nameing convention in the 'File n.' cells, the program searches
    the users computer for the files, and performs the calculated absoption
    of that measurement, taking into account the power drift from the PCI
    calibration paramaters where P@Max is also recorded.

    A linear interpolation between calibration points is carred out to
    calculate the total error due to calibration drift the logic used in
    cdrift.m
    see cdrift.m

    If conversion of absorption in to 'k' values is required, after the
    initial analysis, the user will be prompted to continue by a [Y/N]
    request. Where if enabled, the same measuremnets will be used to call
    codes written by Z.Tornasi and S.Leavey to calculate the Electric feild
    intensity inside the coating layer for a given paramater space.

    


    [final_output]    = abs_analysis(xl_range,measurement_type,bulk)

    Example:
    [final_output]    = abs_analysis('A12:S18','Point','bulk')

    INPUTS

     xl_range                            : Range of cells in a given excel sheet which will be read
                                           into matlab for anaylsis. Must be formatted as in
                                           [row0,row1,col0,col1,1st measurement, last measurement]

    
    Parameter Space                      : [Specifiy Pump Wavelength (nm),Substrate Refractive index at Probe wavelength,
                                            Substrate thickness (mm),
                                            Min Coating Thickness (nm),Max Coating Thickness (nm),
                                            Min Coating Refractive index,Max Coating Refractive index]

     measurement_type                    : String input, takes in 'Point' or 'Map' to specify the
                                           type of absorption measurement carried out.
                                           For mapping funciton calls
                                           Map_power_Correction_PY_V5.m  which requires python
                                           inputs.


     bulk                                : String input specifying weather there is significant
                                           absorotion contributions from the samples substrate.
                                           Enabling this will take the substrate absorption
                                           into account where applicable.


     OUTPUTS

     final_output                       : Tabulated data containing new calculated absorption
                                          values based on P@Max valeus where applicable, if not
                                          provided will return origional input values.
                                          Calculated errors from calibration drift.
                                          K and QWL normallised absorption values if requested.

     K_.fig                             : Paramater space of k values for given input thickness
                                          and refracive index of sample.

     QWL_.fig                           : Paramater space of QWL values for given input thickness
                                          and refracive index of sample.

     Branches.mat                       : All outputs from k analysis containg power values and
                                          paramater spaces for each measurement analysed.

     outputs.mat                        : Nice outputs from k analysis, which are then displayed
                                          as tabulated data in [final_output]

     corrrected_absorption_(k).txt      : Text file containg [final_output]
                                          data, tab delimited.



    Author  R. Johnston 2022
    """

    # Make new directory for the output files
    path = os.getcwd()
    char_swap = ["\\","/"]
    path = path.replace(char_swap[0][0],char_swap[1])
    time = f'{dt.datetime.now():%d_%m_%y_%H%M}'

    pathfull = path + '/' +time
    os.makedirs(pathfull) # Will break if the directory already exists

    # Evaluate the analysis depending on the type of measurement
    
    if measurement_type=='Point':
        print('Point Measurement')

    abs_dic = {}

    excel_fname = path + '/' + fname # Uses the same root folder
    xl_sheet = xl.load_workbook(excel_fname, data_only=True)

    [row0,row1,col0,col1,scan0,scan1] = xl_range

    # Load data from excel sheet & files
    for x in range(row0,row1):
        name = str(xl_sheet['Sheet1'].cell(row=x,column=col0).value)
        print('scanning over: ',name)
        values = np.asarray([[xl_sheet['Sheet1'].cell(row=x,column=v).value] for v in range(col0+1,col1) ])
        values_mod = np.asarray([float(x) for x in values[0][0].split(',')])
        for y in range(1,len(values)):
            values_mod = np.append(values_mod,values[y])

        #Load from files
        try:
            scan_data = np.asarray(np.loadtxt(data_loc+'\\'+name+'.txt',delimiter='\t',dtype=float,usecols=(0,3,4)))
        except:
            print('Error 1: no file found at {}'.format(data_loc+'\\'+name+'.txt'))

        # Make power corrections for the data (always on by default)
        if len(str(values_mod[-1]))==0:
            print('Warning 1: no power corrections applied to {0}'.format(name))
            values_mod = np.append(values_mod,float(values_mod[3]))
            # Append now trans & abs/Trans to each file entry
            Trans = (float(values_mod[5])/float(values_mod[4]))
            values_mod = np.append(values_mod,[Trans,float(values_mod[-1])/Trans])
            
        else:
            scan_data[:,1] = scan_data[:,1]*(float(values_mod[-2])/float(values_mod[-1]))
            values_mod = np.append(values_mod,float(values_mod[3])*(float(values_mod[-2])/float(values_mod[-1])))
            # Append now trans & abs/Trans to each file entry
            Trans = (float(values_mod[5])/float(values_mod[4]))
            values_mod = np.append(values_mod,[Trans,float(values_mod[-1])/Trans])
             
        abs_dic[name] = [values_mod,scan_data]
    
    calib_parameters = []
    for x in range(row0,row0+6):
        calib_parameters.append(xl_sheet['Sheet1'].cell(row=x,column=1).value)
    calib_parameters.pop(4)
    return [abs_dic,calib_parameters]


def evaluate_k(paramter_space,Resolution):

        """
        NEXT Version:

        1. Assume that the light in the substrate is incoherent with the slab thickness >> wavelength
           such that slight variations of few wvs prevent interference occuring in this layer.
           Also, with tmm, this layer will not contribute any absorption.

        2. Comments from Sebastians python code
           # TODO: this needs to be combined with matrix calculations can't be
                    that difficult! As original code is all very simple.
           # TODO: AOI is not taken into account for now, as well as s/p distinction


        Function used to estimate the optical absorption of a single thin film coating
        layer by calculating the total electric feild strength inside the coating

        INPUTS

        Parameter Space                      : [Specifiy Pump Wavelength (nm),Substrate Refractive index at Probe wavelength,
                                                Substrate thickness (mm),
                                            Min Coating Thickness (nm),Max Coating Thickness (nm),
                                            Min Coating Refractive index,Max Coating Refractive index]

        Resolution                           : Number of points to be used in the paramter space

        
        Author Z.Tornasi & S. Tait 2020
        Ported to python by R. Johnston 2022
        """
        n_out= 1 # refractive index outside coated substrate is air and so always 1

        # Matrix evaluation across the many different possible combinations of n & t values
        # Make use of tmm python package here
        k = 1e-5 # temporary k value
        n_list = [n_out] + [complex(paramter_space[1],k)] + [n_out]
        t_list = ['inf'] + [paramter_space[2]] + ['inf']    
        
        return 0

def a_to_kay(a,n,wv0):

        """
        Caluclate the true k by integrated electric field over the thickness of the layer

        """
        a_scaled = a
        kay = wv0*a_scaled*1e-6 / (4*m.pi)

        return kay

# Equations for siliocn transmission models 

def a2k(absorption,wv,direction='forward'):

    """
    Caluclate the true k by integrated electric field over the thickness of the layer
    
    INPUTS
    a: cm^-1
    wv: m
    """
    if direction == 'forward':
      a = absorption*1e2 # convert to m^-1
      k = a*(wv/(4*m.pi))
      return k
    elif direction == 'reverse':
      a = 4*m.pi*absorption/wv #m-1
      a = a*1e-2
      return a
    else:
      print('Please specify either forward or reverse direction')
      return None

def alpha(A,l):
    """
    A: Absorbance [%/cm]
    a = cm^-1

    Breaks when A > 1? 
    """
    return -m.log(1-(A*l))/l

def a2A(a,l):
    return (np.power(m.e,a*l))/l # surely it cant be neagtive?   

# Photothermal equations by setup

## Deflection

# Class for handling and displaying the deflection data



class Deflection_analysis():

    def __init__(self,deflection_database,deflection_data_path):
        """ Initialises the deflection analysis class

        Parameters
        ----------
        ptd_database : dict
            The PTD database
        ptd_data_path : str
            The path to the PTD data folder
        
        """
        self.deflection_database = deflection_database
        self.deflection_data_path = deflection_data_path
        self.plot_options = {}
    
    def plot_add_options(self,plot_options):
        """
        Adds options to the plot options dictionary
        
        Parameters
        ----------
        plot_options : list of str
            The options to be added to the plot options dictionary
            save : str
        """
        for setting in plot_options:
            self.plot_options[setting] = True
        return

    def plot_handle_options(self):
        for key in self.plot_options:
            if self.plot_options[key] == True:
                if key == 'save':
                    print('prepared to save plot')
                    #self.plot_options[key] = self.deflection_data_path + '\\deflection_plot.png'
        return
                    

    def query_database(self,sample=None, wavelength=None, dates=None,scan_type='1D'):
        """ Fetches data/ infor from the PTD database
    
        Parameters
        ----------
        sample : int [optional]
            The sample number of the data to be fetched
        wavelength : int [optional]
            The wavelength of the data to be fetched
        dates : int or list
            The dates of the data to be fetched (e.g. 20230303) in the format YYYYMMDD
        
        Returns
        -------
        data : dict
            The data requested from the PTD database
        
        """
        # Lets just set up a standard search for when all of the handles are given
        if scan_type == '2D':
            prefix = '2D_Map'
        else:
            prefix = ''

        # Query the sample for the correct handles
        self.sample_name = sample
        self.sample_properties = self.deflection_database['samples'][sample]
        self.material_properties = self.deflection_database['materials'][self.sample_properties['material']]
        self.n = self.material_properties['n']
        self.dates = dates

        if wavelength != None and dates != None and sample != None:
                    
            data_dic = {}

            if type(wavelength) == int:
                wavelength = [wavelength]
            elif type(wavelength) != list:
                raise TypeError('Wavelength must be an int or list of ints')
                return
            
            if type(dates) == int:
                dates = [dates]
            elif type(dates) != list:
                raise TypeError('Date must be an int or list of ints')
                return
            

            # Lets iterate over the supplied wavelengths
            for wv in wavelength:
                # Lets iterate over the supplied dates
                for day in dates:
                    try:
                        data_wv = self.deflection_database[wv]
                    except:
                        print('!!! Wavelength: {0} not found in database'.format(wv))
                        continue
                    try:
                        data_date = data_wv[self.sample_properties['type']][day]
                    except:
                        print('!!! Date: {0} not found in database'.format(day))
                        continue
                    for scan in data_date:
                        scan_data = data_date[scan]
                        if scan_data['sample_name'] == sample and scan_data['scan_type'] == scan_type:
                            scan_name = str(day) + '-' + str(scan)
                            data_dic[scan_name] = scan_data
                            data_dic[scan_name]['wavelength'] = wv
                            beam_position = mat(data_dic[scan_name]['beam_origin'])
                            sample_rotation = data_dic[scan_name]['rotation']
                            if data_dic[scan_name]['facing'] == 'front':
                                beam_position = beam_position.T*mat([[m.cos(sample_rotation), m.sin(sample_rotation)],[-m.sin(sample_rotation), m.cos(sample_rotation)]]) # Counter clockwise rotation
                            elif data_dic[scan_name]['facing'] == 'back':
                                beam_position = beam_position.T*mat([[m.cos(sample_rotation), -m.sin(sample_rotation)],[m.sin(sample_rotation), m.cos(sample_rotation)]]) # Clockwise rotation
                            data_dic[scan_name]['beam_position'] =  beam_position
                            scan_path = os.path.join(self.deflection_data_path,prefix+scan_name+'s.nat')
                            data_dic[scan_name]['file_path'] = ''
                            if scan_type == '2D':
                                # This is a 2D measurement, so I need to load the data differently
                                try:
                                    with open(scan_path, 'r') as f:
                                        lines = f.readlines()
                                    data_dic[scan_name]['file_path'] = scan_path
                                except:
                                    print('No scan file found at {0}'.format(scan_path))
                                    continue

                                pattern = r'\]'
                                sep = np.asarray([i for i,line in enumerate(lines) if re.findall(pattern, line)==[']']])
                                test = np.asarray([x/(i+1) for i,x in enumerate(sep[1:]/sep[1])])
                                uneven_steps = len(np.where(test!=1)[0])                  
                                if uneven_steps > 0:
                                    print('Uneven number of steps in z detected, please check data file')
                                z_steps = (sep[1]-sep[0])-2
                                try:
                                    with open(scan_path, 'r') as f:
                                        # To load the file I need to know What the number of steps are in z
                                        lines = (line for i, line in enumerate(f) if i!=0 and (i+1) % (z_steps+2) != 0)
                                        lines_short = (line for i, line in enumerate(lines) if (i+1) % (z_steps+1) != 0)
                                        full_scan_data = np.asarray(np.genfromtxt(lines_short))
                                        # Tidy up the motor positions
                                        full_scan_data[:,0] = self.tidy_spacing(full_scan_data[:,0],sig=3)
                                        full_scan_data[:,1] = self.tidy_spacing(full_scan_data[:,1],sig=3)
                                        if 'power_correction' in scan_data:
                                            full_scan_data[:,2] = full_scan_data[:,2]*scan_data['power_correction']
                                            print('Power correction applied')
                                except:
                                    print('Error loading file! Make sure there is the same number of rows for each step ')
                                    full_scan_data = [0]

                            else:
                                try:
                                    full_scan_data = np.loadtxt(scan_path)
                                    full_scan_data[:,0] = self.tidy_spacings(full_scan_data[:,0],sig=3)
                                    if 'power_correction' in scan_data:
                                        full_scan_data[:,1] = full_scan_data[:,1]*scan_data['power_correction']
                                    data_dic[scan_name]['file_path'] = scan_path
                                except:
                                    print('No 1D scans found at {0}. Continuing onto different scan'.format(scan_path))
                                    continue
                                sample_length = self.sample_properties['length'] # mm
                                sample_length_scale = sample_length/self.n
                                effective_sample_length = round(sample_length_scale) + 5 # mm is rough just now
                                ind_cutoff = np.where(full_scan_data[:,0] > full_scan_data[:,0][0] + effective_sample_length)[0][0]
                                full_scan_data = full_scan_data[:ind_cutoff,:]
                                #full_scan_data[:,1]*= sample_specs[sample]['probe_correction'] # !Probe correction not neeed as Angus explained
                            
                            data_dic[scan_name]['scan_data'] = full_scan_data
            self.data_dic = data_dic
            return self.data_dic
        else:
            print('Please provide a sample, wavelength and date to query the database. Not updating the cached data dic either')
            return None
        
    def clean_data_dic(self):
        """
        Removes the files called for in query_database that are not present in the data folder
        """
        data_dic_dummy  = self.data_dic.copy()
        for scan in data_dic_dummy:
            if self.data_dic[scan]['file_path'] == '':
                del self.data_dic[scan]
        return self.data_dic
    
    def filter_data_dic(self,remove=None):
        data_dic_dummy  = self.data_dic.copy()
        if type(remove) == str:
            for scan in data_dic_dummy:
                for label in data_dic_dummy[scan]:
                    if data_dic_dummy[scan][label] == remove:
                        del self.data_dic[scan]
        return self.data_dic

    def average_data_dic(self,SNR=False):
        # Average the data into one dictionary
        # Make sure you have called the filter_data_dic function first to make sure that all scans are overlapping through
        data_dic_dummy = self.data_dic.copy()
        data_list = []
        for i, scan in enumerate(data_dic_dummy):
            data = data_dic_dummy[scan]['scan_data']
            if i == 0:
                scan_ref = scan
                size_ref = data.size
            if data.size != size_ref:
                print('Scan {0} has a different number of points to the reference scan. Skipping this scan'.format(scan))
            else:
                data_list.append(data)
        data_avg = np.mean(data_list, axis=0)
        err = np.std(data_list, axis=0)
        err = np.where(err==0, 1, err) # Stops the SNR evaluation below from breaking
        print(err)
        self.data_dic['average'] = {}
        if SNR:
            self.data_dic['average']['scan_data'] = data_avg/err
        else:
            self.data_dic['average']['scan_data'] = data_avg
        for label in data_dic_dummy[scan_ref]:
            if label != 'scan_data':
                self.data_dic['average'][label] = data_dic_dummy[scan_ref][label]
        return err

    def tidy_spacing(self,x, sig=2):
        if type(x) == np.ndarray:
            x[np.where(x==0)] = 0.0001 # Stops the log10 from breaking
            x = [round(xi, sig-int(m.floor(m.log10(abs(xi))))-1) for xi in x]
            for i,xi in enumerate(x):
                if abs(xi)<2*pow(10,-sig):
                    x[i] = 0.0
        elif type(x) == list:
            print('Please provide a numpy array')
        else:
            print('number supplied')
            x = round(x, sig-int(m.floor(m.log10(abs(x))))-1)
        return x
    
    def plot_1d(self,def_bounds=[0,1],phase_bounds=[-180,180],data_dic=None,comment=True,lat_skips=1,index_choice_2D=None):
        # Cant switch between plot types in same cell evaluation

        if not hasattr(self,'data_dic'):
            print('No data has been loaded yet. Please run query_database() first')
            return None
        """
        for scan_name in self.data_dic:
          if self.data_dic[scan_name]['scan_type'] != '1D':
              # Extend this to be able to plot a 2D scan in 1D
              print('1D plotting is only available for 1D scans.')
              return None
        """
        self.plot_comment = comment
        self.plot_dimmension = '1D'

        self.prepare_fig()

        front_count = 0
        front_peak_avg = 0
        point_averaged_def_list = []

        if data_dic is None:
            data_dic = self.data_dic
        
        for i,scan_name in enumerate(data_dic):
            if i ==0:
                scan_type = data_dic[scan_name]['scan_type']
            if type(index_choice_2D) == int and i != index_choice_2D:
                continue
            print('Plotting scan: ', scan_name)
            scan_details  = data_dic[scan_name]
            scan_data = scan_details['scan_data']
            beam_origin = data_dic[scan_name]['beam_origin']
            rot = data_dic[scan_name]['rotation']
            if scan_type == '2D':
                lat_positions = np.unique(scan_data[:, 0])
                lat_max = []
                for i,lat_pos in enumerate(lat_positions[lat_skips:]):
                    lat_pos_data = scan_data[np.where(scan_data[:, 0] == lat_pos)]
                    front_peak_avg += np.max(lat_pos_data[:,2])
                    print('plotting lat_pos: ', lat_pos)
                    #list_max.append([lat_pos_data[:,1][np.argmax(lat_pos_data[:,2])],np.max(lat_pos_data[:,2])])
                    self.ax1.plot(lat_pos_data[:,1]*self.n,lat_pos_data[:,2], label='lat_pos: '+str(round(lat_pos,3)))
                    self.ax2.plot(lat_pos_data[:,1]*self.n,lat_pos_data[:,3], label='lat_pos: '+str(round(lat_pos,3)))
                    lat_max.append(np.max(lat_pos_data[:,2]))
                    #ax[i].set_ylim(0,4e-3)
                    if i==0:
                        point_averaged_def = lat_pos_data[:,2]
                        point_averaged_def_list.append(lat_pos_data[:,2])
                        point_averaged_phase = lat_pos_data[:,3]
                    else:
                        point_averaged_def += lat_pos_data[:,2]
                        point_averaged_def_list.append(lat_pos_data[:,2])
                        point_averaged_phase += lat_pos_data[:,3]
                    front_count += 1
            else:
                front_count += 1
            #ax[i].plot(lat_positions,lat_max, label='lat_pos: '+str(lat_pos))
        
        print('Front count: ', front_count)
        print(np.asarray(point_averaged_def_list).shape)
        front_peak_avg = front_peak_avg/front_count
        point_averaged_def = point_averaged_def/front_count
        point_averaged_phase = point_averaged_phase/front_count
        
        self.ax1.plot(lat_pos_data[:,1]*self.n,point_averaged_def, color='k',linestyle='dashed',label='Point averaged deflection')
        #a = np.mean(np.asarray(point_averaged_def_list),axis=0)
        #a = np.sum(np.asarray(point_averaged_def_list),axis=0)/(front_count+2)
        #self.ax1.plot(lat_pos_data[:,1],a, color='r',linestyle='dashed',label='Point averaged deflection')
        point_averaged_def_peak = np.max(point_averaged_def)
        self.ax2.plot(lat_pos_data[:,1]*self.n,point_averaged_phase, color='k',linestyle='dashed',label='Point averaged phase')

        self.ax1.set_ylim(def_bounds)
        self.ax2.legend(bbox_to_anchor=(1.5, 1), loc='upper right', borderaxespad=0.)
        self.ax2.set_ylim(phase_bounds)

        
        if self.plot_comment:
            print('Front peak average: ', front_peak_avg)
            print('Point averaged deflection peak: ', point_averaged_def_peak)

        return np.asarray(point_averaged_def_list)

    def plot_3d(self,elev=30,azim=30,def_bounds=[0,1],phase_bounds=[-180,180],absorption=False,comment=True):
        
        """
        input:
        elev: elevation angle in the z plane
        azim: azimuthal angle in the x,y plane
        def_bounds: deflection bounds for the plot
        phase_bounds: phase bounds for the plot
        absorption: '%cm-1' or 'cm-1' or 'ppm cm-1'
        """
        if not hasattr(self,'data_dic'):
            print('No data has been loaded yet. Please run query_database() first')
            return None
        
        for scan_name in self.data_dic:
          if self.data_dic[scan_name]['scan_type'] != '2D':
              print('3D plotting is only available for 2D scans.')
              return None

        self.elev = elev
        self.azim = azim
        self.def_bounds = def_bounds
        self.phase_bounds = phase_bounds
        self.plot_comment = comment
        self.plot_dimmension='3D'
        
        self.prepare_fig()

        # rotate the data around the z-axis
        front_scan_count = 0
        self.ax1.view_init(elev=self.elev, azim=self.azim)
        self.ax2.view_init(elev=self.elev, azim=self.azim)
        for i,scan_name in enumerate(self.data_dic):
            scan_details  = self.data_dic[scan_name]
            scan_data = scan_details['scan_data']
            if self.plot_comment:
                def_max = scan_data[:,2].max()
                def_min = scan_data[:,2].min()
                phase_max = scan_data[:,3].max()
                phase_min = scan_data[:,3].min()
    
                def_percentage_above = (np.where(scan_data[:,2] > self.def_bounds[1])[0].size/scan_data[:,2].size)*100
                def_percentage_below = (np.where(scan_data[:,2] < self.def_bounds[0])[0].size/scan_data[:,2].size)*100
                phase_percentage_above = (np.where(scan_data[:,3] > self.phase_bounds[1])[0].size/scan_data[:,3].size)*100
                phase_percentage_below = (np.where(scan_data[:,3] < self.phase_bounds[0])[0].size/scan_data[:,3].size)*100
             
                print('Deflection percentage below {0} is {1:.2f}%'.format(self.def_bounds[0],def_percentage_below))
                print('Deflection percentage above {0} is {1:.2f}%'.format(self.def_bounds[1],def_percentage_above))
                print('Deflection min: {0} um, max: {1} um'.format(def_min,def_max))

                print('Phase percentage above {0} is {1:.2f}%'.format(self.phase_bounds[1],phase_percentage_above))
                print('Phase percentage below {0} is {1:.2f}%'.format(self.phase_bounds[0],phase_percentage_below)) 
                print('Phase min: {0} deg, max: {1} deg'.format(phase_min,phase_max))         

            beam_origin = self.data_dic[scan_name]['beam_origin']
            rot = self.data_dic[scan_name]['rotation']
            if scan_details['facing'] != 'fdront':  # temportary edit to compare front and back simply by eye for now       
                x = np.unique(scan_data[:, 0])
                x = x - x.min()
                x = x - x.max()/2
                #x = x + beam_origin[0]
                z = np.unique(scan_data[:, 1])*self.n
                z = z - z.min()
                scan_depth = z.max() - z.min()
                y = np.zeros_like(x) # for zero offset & rotation
                # Correct for tilt if required
                if 'tilt' in scan_details:
                    dir = 'clockwise'
                    # Need to rebroadcast x and z to match the shape of the tilt array
                    # Then I need to recast the x back to being the same length as y
                    """
                    resample_factor = 0.5
                    spacing = resample_factor*np.diff(x)[0]
                    if np.diff(x)[0]%spacing!=0.0:
                        print('New list does not contain the old list')
                    np.arange(min(x), max(x), spacing)
                    """
                    #x,z = self.rotate(x,z,scan_details['tilt'],dir=dir)
                dir = 'clockwise'
                x,y = self.rotate(x,y,rot+180,dir=dir) # 180 required to get the later translation to match up with sample position
                x_offset,y_offset = self.rotate(beam_origin[0],beam_origin[1],rot+180)
                x = x - x_offset
                y = y - y_offset
                if self.plot_comment:
                    print('Beam origin: ', beam_origin)
                    print('Rotation: ', rot)
                    print('x: ', x)
                    print('y: ', y)
                    print('z: ', z)
                    print('Scan depth: ', scan_depth)
                Z,X = np.meshgrid(z,x)
                #print('X,Z shapes',X.shape, Z.shape)
                Z,Y = np.meshgrid(z,y)
                #print('Y,Z shapes',Y.shape, Z.shape)
                x_min = x.min()
                x_max = x.max()
                y_min = y.min()
                y_max = y.max()
                z_min = z.min()
                z_max = z.max()
                extent = [x_min, x_max, y_min, y_max, z_min, z_max]
                mesh_resolution = 1
                mesh_size_x = (x[1] - x[0])/mesh_resolution
                mesh_size_y = (y[1] - y[0])/mesh_resolution
                mesh_size_z = (z[1] - z[0])/mesh_resolution
                deflection = scan_data[:, 2].reshape(len(x), len(z))
                phase = scan_data[:, 3].reshape(len(x), len(z))
                #print(deflection.shape)

                a_ref = 5e4 # Reference absorption
                deflection_ref = 1.8e-3
                
                # Show absorption instead if requested
                if absorption=='%cm-1':
                    # print showing absorption instead
                    a_ref = 5e4 # Reference absorption
                    deflection_ref = 1.8e-3
                    deflection = deflection*a_ref/(deflection_ref*1e4)
                    if i == 0:
                        self.def_bounds = list(np.asarray(self.def_bounds)*a_ref/(deflection_ref*1e4))
                    print('Showing absorption instead of deflection')
                    print('Absorption bounds: ', self.def_bounds)
                
                elif absorption=='ppm cm-1':
                    a_ref = 5e4 # Reference absorption
                    deflection_ref = 1.8e-3
                    deflection = deflection*a_ref/(deflection_ref)
                    if i == 0:
                        self.def_bounds = list(np.asarray(self.def_bounds)*a_ref/(deflection_ref))
                    print('Showing absorption instead of deflection')
                    print('Absorption bounds: ', self.def_bounds)

                norm_def = colors.Normalize(vmin=self.def_bounds[0], vmax=self.def_bounds[1]) # Set the normalization range based on min & max of a single 2d scan
                face_colors_def = plt.cm.inferno(norm_def(deflection)) # Apply normalisation to the single scan data
    
                norm_phase = colors.Normalize(vmin=self.phase_bounds[0], vmax=self.phase_bounds[1]) # Set the normalization range based on min & max of a single 2d scan
                face_colors_phase = plt.cm.inferno(norm_phase(phase)) # Apply normalisation to the single scan data
                
                #Plot deflection     
                surf = self.ax1.plot_surface(Z,X, Y, facecolors=face_colors_def, shade=False)
                surf.set_clim(self.def_bounds[0], self.def_bounds[1])
                surf.set_cmap('inferno')
                #ax1.set_xticks(np.arange(x_min, x_max + mesh_size_x, mesh_size_x))
                #ax1.set_yticks(np.arange(y_min, y_max + mesh_size_y, mesh_size_y))
               
                # Plot Phase
                surf_phase = self.ax2.plot_surface(Z,X,Y, facecolors=face_colors_phase, shade=False)
                surf_phase.set_clim(self.phase_bounds[0], self.phase_bounds[1])
                surf_phase.set_cmap('inferno')
                #ax2.set_xticks(np.arange(x_min, x_max + mesh_size_x, mesh_size_x))
                #ax2.set_yticks(np.arange(y_min, y_max + mesh_size_y, mesh_size_y))

                if front_scan_count==0:
                    # To save me trying to scale colorbars properly I am just going to plot the first one just now
                    self.fig.colorbar(surf, shrink=0.5, aspect=1,cax=self.fig.add_axes([0, 0.3, 0.03, 0.5]))
                    self.fig.colorbar(surf_phase, shrink=0.5, aspect=1,cax=self.fig.add_axes([0.93, 0.3, 0.01, 0.5]),norm=norm_phase)
        
        sample_thickness = self.sample_properties['length']
        sample_radius = self.sample_properties['diameter']/2
        z_start = 0
        if self.sample_properties['shape'] == 'cylinder':
            Xc,Yc,Zc = self.data_for_cylinder_along_z(0,0,z_start,sample_radius,sample_thickness)
            self.ax1.plot_surface(Zc, Xc , Yc, alpha=0.1)
            self.ax2.plot_surface(Zc, Xc , Yc, alpha=0.1)
        elif self.sample_properties['shape'] == 'cuboid':
            print('Cuboid plotting not yet implemented')
        return
    
    def prepare_fig(self):
        if hasattr(self,'fig'):
            # Clear figure if its there
            self.fig.clear()
        
        self.fig = plt.figure(figsize=(10, 10))
        self.fig.tight_layout()

        if self.plot_dimmension=='1D':
            plt.subplots_adjust(left=0.13, right=0.87, bottom=0.2, top=0.8, wspace=0.4)
            self.ax1 = self.fig.add_subplot(121)
            self.ax2 = self.fig.add_subplot(122)
            self.fig.suptitle('1D deflection map of {0}, {1}'.format(self.sample_name,str(self.dates)))
            self.ax1.set_xlabel('Longitudinal axis [mm]')
            self.ax1.set_ylabel('Deflection')
            self.ax1.set_title('Deflection')
            self.ax2.set_xlabel('Longitudinal axis [mm]')
            self.ax2.set_ylabel('Phase [degrees]')
            self.ax2.set_title('Phase')

        elif self.plot_dimmension=='3D':
            plt.subplots_adjust(left=0.13, right=0.87, bottom=0.2, top=1, wspace=0.2)
            self.ax1 = self.fig.add_subplot(121, projection='3d')
            self.ax2 = self.fig.add_subplot(122, projection='3d')
            self.fig.suptitle('3D deflection map {0}, {1}'.format(self.sample_name,str(self.dates)))
            self.ax1.set_xlabel('Longitudinal axis [mm]')
            self.ax1.set_ylabel('lateral axis [mm]')
            self.ax1.set_zlabel('height [mm]')
            self.ax1.set_title('Deflection')
            self.ax2.set_xlabel('Longitudinal axis [mm]')
            self.ax2.set_ylabel('lateral axis [mm]')
            self.ax2.set_zlabel('height [mm]')
            self.ax2.set_title('Phase [degrees]')
            
    
    def rotate(self,x, y, theta,dir='clockwise'):
        # Convert degrees to radians
        theta = np.radians(theta)
        # Compute sin and cos of the angle
        cos_theta = np.cos(theta)
        sin_theta = np.sin(theta)
        # Perform the rotation
        if dir=='counter-clockwise':
          x_rotated = x * cos_theta - y * sin_theta
          y_rotated = x * sin_theta + y * cos_theta
        elif dir=='clockwise':
            x_rotated = x * cos_theta + y * sin_theta
            y_rotated = -x * sin_theta + y * cos_theta
        return x_rotated, y_rotated

    def data_for_cylinder_along_z(self,center_x,center_y,center_z,radius,height_z):
        z = np.linspace(center_z, center_z+height_z, 50)
        theta = np.linspace(0, 2*np.pi, 50)
        theta_grid, z_grid=np.meshgrid(theta, z)
        x_grid = radius*np.cos(theta_grid) + center_x
        y_grid = radius*np.sin(theta_grid) + center_y
        return x_grid,y_grid,z_grid

    def calculate_tilt(self,scan_index=0,lat_index_window=[0,-1]):
        """
        Calculates the tilt of the sample by looking at the hift in longitudinal scan peak as a function of lateral position
        
        Only works for 2D surface scans where the peaks are distinct
        """
        long_max = []

        for i,scan_name in enumerate(self.data_dic):
            if i==scan_index:
                if self.data_dic[scan_name]['scan_type'] == '2D':
                    if self.data_dic[scan_name]['sample_name'] == 'AB9':
                        # Only works for 2D AB9 at the moment
                        scan_data = self.data_dic[scan_name]['scan_data']
                        lat_positions = np.unique(scan_data[:, 0])
                        for i,lat_pos in enumerate(lat_positions[lat_index_window[0]:lat_index_window[1]]):
                            lat_pos_data = scan_data[np.where(scan_data[:, 0] == lat_pos)]
                            long_max.append(lat_pos_data[:,1][np.argmax(lat_pos_data[:,2])]*self.n)
                    else:
                        print('Sample not supported. Please choose and AB9 sample scanned in 2D for now')
                        return
        tilt = np.degrees(np.arctan((long_max[-1]-long_max[0])/(lat_positions[lat_index_window[1]]-lat_positions[lat_index_window[0]])))
        return tilt
    

""" bukshtabAppliedPhotometryRadiometry2012

""" 

def def_am_pump_limited(dndT,P,w,rho,Cp,r0,alpha,l,xm):
    """
    Amplitude modulation factor
    Input:
        dndT: dn/dT
        P: Pump power [W]
        w: Modulation frequency [Hz]
        rho: Density [kg/m^3]
        Cp: Specific heat [J/(kg*K)]
        r0: Probe beam radius [m]
        alpha: Absorption coefficient [1/m]
        l: Sample thickness [m]
        xm: Modulation amplitude [m]
    Output:
        def_am: Amplitude modulation factor
    """
    def_am = ((dndT*P)/(rho*Cp*w*np.power(np.pi*r0,2)))*(1-np.exp(-alpha*l))*(-2*(xm/np.power(r0,2))*np.exp(-np.power(xm/r0,2)))
    return def_am

def def_am_diff_limited(dndT,P,lamT,r0,alpha,l,xm):
    """
    Amplitude modulation factor
    Input:
        dndT: dn/dT
        P: Pump power [W]
        lamT: Thermal diffusion length [m]: calculated from the thermal diffusion coefficient and supplied as input likely from an empirical relation
        r0: Probe beam radius [m]
        alpha: Absorption coefficient [1/m]
        l: Sample thickness [m]
        xm: Modulation amplitude [m]
    Output:
        def_am: Amplitude modulation factor
    """
    def_am = ((dndT*P)/(lamT*np.power(np.pi,2)*xm))*(1-np.exp(-alpha*l))*(1-np.exp(-np.power(xm/r0,2)))
    return def_am



"""
Boccara [10.1364/AO.20.001333], [10.1364/AO.42.000649]
CW modulation with the following constraints:
1. Diffusion length smaller than the sample
    a. Point probe beam
    b. Infinite radial substrate
    c. Chopped signal
2. Collimated pump, effectively over the probes confocal distance.
3. Substrate only deflection, neglecting air effects in surrounding medium for off oblique beams. Not expected to have a notable effect.

"""

def mu(D,w):
    """
    Thermal diffusion length from a single modulation cycle
    Input:
        D: Diffusion coefficient
        w: Modulation frequency [Hz]
    Output:
        mu: Thermal diffusion length
    """
    mu = np.sqrt(2*D/w)
    return mu

def def_am(x,waist):
    """
    Amplitude modulation factor
    Input:
        x: Position
        waist: Beam waist
    Output:
        g: Amplitude modulation factor

    """
    g = m.exp(-2*pow(x/waist,2))*complex(0,-1)*8*x/pow(waist,3)
    return g

def def_pm(x,x0,waist,inf_ap=100):
    """
    position modulation factor
    Input:
        x: Position
        x0: modulation amplitude
        waist: Beam waist
    Output:
        g: Position modulation factor
    """
    p = np.linspace(0,inf_ap,inf_ap+1)
    bessel1 = np.power([-1]*(inf_ap+1),p)*(sp.iv(p,pow(x0/waist,2))-sp.iv(p+1,pow(x0/waist,2)))
    bessel2 = x0*sp.iv(2*p,4*x0*x/pow(waist,2)) - 2*x*sp.iv(2*p+1,4*x0*x/pow(waist,2)) + x0*sp.iv(2*p+2,4*x0*x/pow(waist,2))
    bes = np.sum(bessel1*bessel2)
    g = (complex(0,1)*8/pow(waist,2))*m.exp(-2*pow(x/waist,2))*m.exp(-pow(x0/waist,2))*bes
    return g

def def_phi_1(dndT,n,alpha,P0,D,kappa,w,beta,g):
    """
    Angular Deviation inside the sample
    Input:
        dn/dT: Refractive index temperature coefficient
        n: Refractive index
        alpha: Absorption coefficient
        P0: Pump power
        D: Thermal diffusion coefficient
        kappa: Thermal conductivity
        w: Modulation frequency
        beta: Beam angular seperation
        g: Amplitude modulation factor
    Output:
        phi: Angular deviation
        
    """
    phi = (-dndT/n)*(alpha*P0*D)/(m.sqrt(2*m.pi)*kappa*w*m.sin(beta))*g

    return phi

def def_phi_2(dndT,n,alpha,P0,rho,C,w,beta,g):
    """
    Angular Deviation inside the sample
    Takes the magnitude of the imaginary component
    Input:
        dn/dT: Refractive index temperature coefficient
        n: Refractive index
        alpha: Absorption coefficient
        P0: Pump power
        rho: Density
        C: Specific heat capacity
        w: Modulation frequency
        beta: Beam angular seperation
        g: Amplitude modulation factor
        Output:
        phi: Angular deviation
    
    Does look good
    """
    phi = abs(((-dndT/n)*(alpha*P0)/(m.sqrt(2*m.pi)*rho*C*w*m.sin(beta))*g).imag)
    return phi

def rect(arr,thresh,setv):
    return np.where(np.abs(arr)<=thresh, setv, 0)

def mod(t,fch,ds,w1,N):
    if type(t)==int:
        arr = []
        for j in range(0,N+1):
            pt1 = .5*(1+m.erf((pow(2,.5)*ds/w1)*(2*fch*t-(2*j +.5))))*rect(2*fch*t-2*j-.5,.5,1)
            pt2 = .5*(1+m.erf((pow(2,.5)*ds/w1)*(2*fch*t-(2*j +1.5))))*rect(2*fch*t-2*j-1.5,.5,1)
            arr.append(pt1+pt2)
        tot = np.sum(arr)
    else:
        # For an array of t values
        tot = []
        for i in t:
            arr = []
            for j in range(0,N+1):
                pt1 = .5*(1+m.erf((pow(2,.5)*ds/w1)*(2*fch*i-(2*j +.5))))*rect(2*fch*i-2*j-.5,.5,1)
                pt2 = .5*(1+m.erf((pow(2,.5)*ds/w1)*(2*fch*i-(2*j +1.5))))*rect(2*fch*i-2*j-1.5,.5,1)
                arr.append(pt1+pt2)
            tot.append(np.sum(arr))
    return tot

def decay(a,b,I0,z,z1):
    return a/(exp(a*(z-z1))*(a+b*I0) -b*I0)

def def_simp_analytical(mod,dec,I0,size,res,center=None):
    """
    From 1981 Boccarra paper

    Returns the intensity

    EXAMPLE

    fch= 100
    cycles = 100
    tlim = cycles/fch
    t = np.linspace(0,tlim,10*cycles)
    chop = mod(t,fch,1e-4,1e-3,cycles*2)

    """
    x = np.arange(0, size, res, float)
    y = x[:,np.newaxis]
    if center is None:
        x0 = y0 = size // 2
    else:
        x0 = center[0]
        y0 = center[1]
    static = np.exp(-4*np.log(2) * ((x-x0)**2 + (y-y0)**2) /10)
    return static*mod

"""
# Silicon Calibration paramters
n_cal = 3.42
dndT_cal = 1.94e-4 # K−1
alpha_cal = 2.8e-2 #m^-1
lam_cal = 0.1 # Thermal length
D = 87e-6 # Thermal diffusivity (m^2 s-1)
kappa = 1.3e-3 # Thermal conductivity (m k^-1)
#D = 8e3  # Diffusivity (m^2 s^-1)

# Beam parameters
P0 = 1 #W
beta = m.radians(20) # radians
x0 = 1e-5 # seperation between beam maximums (m)
x = 0 # position of pump (m)
w = 2*m.pi*100 # rad*Hz^-1
W = 1e-3 #pump beam radius (m)

# Summation paramters
inf_ap = 100
p = np.linspace(0,inf_ap,inf_ap+1)
"""

## PCI Equations

def PCI_Dispersion(N,dndT,n,k,rho,c,beta,fc,w0,w1,lam1,IS,source='surf'):
    """

    Determine Signal and Phase from initial parameters of the PCI setup

    LIMITATIONS:
    1. No thermal expansion effects, dn/dT only
    Material properties required for calculations:

    INPUTS
    N = 100 # Default number of integration steps for integrate.Quad 
    & For each material:
    dndT : dn/dT at probe wavelength (10-5)  ,coefficient of thermal expansion should be about or smaller than dn/dT to neglect elasto-optic contributions to dn/dT
    n : index of refraction 
    k : thermal conductivity (W/m-K)
    rho : density (kg/m3)
    c : specific heat (J/kg-K )
    
    For setup:
    beta : Crossing angle in radians (Assuming pump is at normal incidence)
    fc : Modulation frequency
    w0 : Pump waist (um)
    w1 : Probe waist (um),  Assuming, I believe, that the both beams cross at their waist
    lam1 : Probe wavelength (nm)
    IS : Distance between crossing point and photodiode surface (mm)

    method : PCI only for now
    source : surf or bulk

    OUTPUTS
    [0] : Signal
    [1] : phase

    VALIDATION

    Reproduces the same results as the mathematica file from Jessica and the scaling mathcad / word files

    Author: Ross Johnston 2021
    """

    #Normalised parameters

    zt = m.pi*(w0)**2/(2*lam1) # rayleigh range
    ep = 0.5*(w0/w1)**2 #rayleigh ratio
    zeta = IS/zt #norm distance
    tau = (w0**2)*(rho*c)*(1/(8*k))*1e-12

    ft = (1/(2*m.pi*tau)) # thermal relaxation freq
    omg = fc/ft #norm freq

    #Photothermal Coeeficient

    if source=='bulk':
        A = m.sqrt(m.pi/8)*(w0/lam1)*(dndT*n/k)*(1/m.sin(beta)) # Photo-thermal coefficient
        Dispersion = lambda tau: ((1/cm.sqrt(1+tau + (zeta/(ep*zeta + complex(0,1))))).imag)*(cm.exp(-complex(0,1)*omg*tau)).real
        Dispersion_im = lambda tau: ((1/cm.sqrt(1+tau + (zeta/(ep*zeta + complex(0,1))))).imag)*(cm.exp(-complex(0,1)*omg*tau)).imag
        D,err = integrate.quad(Dispersion,0,N*(m.pi/omg))
        D_im,err_im = integrate.quad(Dispersion_im,0,N*(m.pi/omg))
        D_abs = abs(complex(D,D_im))
        D_arg = np.angle(complex(D,D_im),deg=True)
    else:
        # i.e source == 'surface'
        A = m.sqrt(m.pi/8)*(w0/lam1)*(dndT/k) # # Photothermal-coefficient
        Dispersion = lambda tau: ((1/(1+tau + (zeta/(ep*zeta + complex(0,1))))).imag)*(cm.exp(-complex(0,1)*omg*tau)).real
        Dispersion_im = lambda tau: ((1/(1+tau + (zeta/(ep*zeta + complex(0,1))))).imag)*(cm.exp(-complex(0,1)*omg*tau)).imag
        D,err = integrate.quad(Dispersion,0,N*(m.pi/omg))
        D_im,err_im = integrate.quad(Dispersion_im,0,N*(m.pi/omg))
        D_abs = abs(complex(D,D_im))
        D_arg = np.angle(complex(D,D_im),deg=True)

    return [A*D_abs,D_arg]

# Photothermal equations

def lam_T(T,w):
    a_t = k_T_poly(T)/(rho*C_T_poly(T))
    lam_t = np.sqrt(a_t/(m.pi*w))
    return lam_t


# Equations for positioning mirrors in optical paths

def ref_vector(v_in, tht,phi,A,rot_order='y-p'):
    # https://www.thorlabs.com/newgrouppage9.cfm?objectgroup_id=12211
    if rot_order == 'y-p':
      # Where the axis is defined relative to that of the incident beam orientation
      x = 2*np.cos(tht)*np.cos(phi)*np.sin(phi)
      y = -2*np.cos(tht)*np.sin(tht)*(np.cos(phi)**2)
      z = 1 - 2*(np.cos(tht)**2)*(np.cos(phi)**2)
    elif rot_order == 'p-y':
      x = 2*(np.cos(tht)**2)*np.cos(phi)*np.sin(phi)
      y = -2*np.cos(tht)*np.sin(tht)*np.cos(phi)
      z = 1 - 2*(np.cos(tht)**2)*(np.cos(phi)**2)
    else:
        raise ValueError("rot_order must be either 'y-p' or 'p-y'")
    v_out = v_in + A*np.array([x,y,z])
    return v_out

def ref_vector_global(v_in, tht,phi,A,rot_order='y-p'):
    # https://www.thorlabs.com/newgrouppage9.cfm?objectgroup_id=12211
    if rot_order == 'y-p':
      v_out = 0
    v_out = 0
    return v_out


# Equations for semiconductor properties

## Band-Band 
def direct_b2b(v_array,n,me_reduced,Eg):
    A = pow(c.e/c.Planck,2)*(1/(n*c.c*me_reduced))*pow(2*me_reduced,1.5)
    alpha_array = A*np.sqrt(c.Planck*v_array-Eg)/(c.Planck*v_array)
    return alpha_array

def indirect_b2b(v_array,n,me_reduced,Eg,T):
    A = pow(c.e/c.Planck,2)*(1/(n*c.c*me_reduced))*pow(2*me_reduced,1.5)
    f_BE = 1/ (np.power(c.e,(c.Planck*v_array)/(c.Boltzmann*T))-1)
    below_Eg = A*f_BE*np.power(2*c.Planck*v_array-Eg,2)
    above_Eg = A*(1-f_BE)*np.power(Eg,2)
    return


# Buffer from old optical_equations.py

# Other equations

def mass_supported_crystalline_neck(d,sig,g):
    """
    Size limits (kg) of a neck pulled Silicon DOI: 10.1016/0022-0248(90)90253-H

    INPUT
    d: diameter of neck
    sig: fracture strength dyn/cm^2 <=> 0.1 Pa
    g: gravity acceleration cm/s^2

    OUTPUT
    size: [kg] # It is clear that with a larger 12mm neck, stabilised against dislocations with B-doping that a 2000kg crystal can be pulled which is x10 above the target of future detectors & so this will not be an issue.

    EXAMPLE
    sig = 2e9 # 0.2 GPa which is fairly standard for Silicon DOI: 10.1016/B978-0-444-82413-4.50060-3
    g = 981
    rho = 2.33e3


    """
    return (sig*c.pi*d**2)/(4*g*1000)